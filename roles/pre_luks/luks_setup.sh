cryptsetup -y -v luksFormat /dev/sdb
dd if=/dev/urandom of=/root/disk bs=512 count=8
cryptsetup luksAddKey /dev/sdb /root/disk 
cryptsetup luksOpen /dev/sdb data --key-file=/root/disk
mkfs.ext4 /dev/mapper/data
mkdir /media/data
mount /dev/mapper/data /media/data

cryptsetup luksDump /dev/sdb | grep "UUID"