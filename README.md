# SibelHealth Gates Server Pre-Deployment Setup

## Requirements
- [Ansible CLI](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Setup Steps
1. Add the host computer's IP address and target ssh port to the `hosts` file, under `[gates]`.
2. Run setup.sh, and follow instructions as prompted