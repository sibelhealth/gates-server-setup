ansible-playbook gates_provision_1.yml -c paramiko --ask-pass --ask-become-pass

ansible-playbook gates_provision_2.yml -c paramiko --ask-pass --ask-become-pass

echo "Please run /luks_setup.sh as sudo on the target machine to setup luks."
read -p "Please set up luks on the target machine before proceeding."

ansible-playbook gates_provision_3.yml -c paramiko --ask-pass --ask-become-pass